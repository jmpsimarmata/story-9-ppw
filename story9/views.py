from django.shortcuts import render
from django.http import JsonResponse
import json
import requests
from .models import Book
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def index(request):
    return render(request, 'index.html')

def data(request):
    try:
        q = request.GET['q']
    except:
        q = 'quilting'
    json_response= requests.get("https://www.googleapis.com/books/v1/volumes?q=" + q).json()

    for i in json_response['items']:
        try:
            book = Book.objects.get(id=i['id'])
            likes = book.likes
        except:
            likes = 0
        i['volumeInfo']['likes'] = likes

    return JsonResponse(json_response)

@csrf_exempt
def addLike(request):
    data_books, created = Book.objects.get_or_create(
        id = request.POST['id'],
        cover = request.POST['cover'],
        title = request.POST['title'],
        author = request.POST['author'],
        publisher = request.POST['publisher'],
    )
    data_books.likes=int(request.POST['likes'])
    data_books.save()
    return JsonResponse(data_books.likes, safe=False)

def top5books(request):
    book = Book.objects.order_by('-likes')[:5]
    lst = []
    for i in book:
        lst.append({
            'id' : i.id,
            'cover' : i.cover,
            'title' : i.title,
            'author' : i.author,
            'publisher' : i.publisher,
            'likes' : i.likes
        })

    data = {
        'top5books' : lst
    }
    return JsonResponse(data, safe=False)