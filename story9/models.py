from django.db import models

# Create your models here.
class Book (models.Model):
    id =  models.CharField(max_length = 200, primary_key = True)
    cover = models.CharField(max_length = 100)
    title = models.CharField(max_length = 100)
    author = models.CharField(max_length=100, blank=True)
    publisher = models.CharField(max_length = 100)
    likes = models.IntegerField(default=0)