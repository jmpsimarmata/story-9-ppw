from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class UnitTestStory9(TestCase):
    def test_app_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_landing_page_is_using_correct_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    def test_landing_page_is_written(self):
        self.assertIsNotNone(index)
    def test_function_story8(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class FunctionalTestStory9(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTestStory9, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTestStory9, self).tearDown()

    def test_functional(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(3)

        search_book = selenium.find_element_by_name('search_box')
        search_book.send_keys('Coronavirus')
        submit_search = selenium.find_element_by_name('search_submit')
        submit_search.send_keys(Keys.RETURN)
        time.sleep(3)

        like_button = selenium.find_element_by_name('likebutton')
        like_button.send_keys(Keys.RETURN)
        time.sleep(2)

        modal_button = selenium.find_element_by_name('namebutton')
        modal_button.send_keys(Keys.RETURN)
        time.sleep(2)

        top_text = selenium.find_element_by_class_name('judul').text
        self.assertIn('Book Search by Johanes', top_text)
        time.sleep(2)