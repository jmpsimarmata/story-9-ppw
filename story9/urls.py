from django.urls import path
from .views import index, data, addLike, top5books

app_name = 'story9'

urlpatterns = [
    path('', index, name='index'),
    path('data/', data, name='data_books'),
    path('addLike/', addLike, name='add_like'),
    path('top5books/', top5books, name='top5books')
]