$(document).ready(function() {
    $("#input").on("click", function(e) {
        var q = e.currentTarget.previousElementSibling.value.toLowerCase()
        console.log(q)
        $.ajax({
            url: data_books_url + q,
            success: function(data) {
                $('#konten').html('')
                var result = '<tr>';
                for (var i = 0; i < data.items.length; i++) {
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='width:22vh' src='" +
                        data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                        "<td class='align-middle'><button id='like' onclick='like()' name='likebutton'>Like this book</button></td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.likes + "</td>" +
                        "<td class='align-middle'>" + data.items[i].id + "</td>"
                }
                $('#konten').append(result);
            },
            error: function(error) {
                alert("Sorry, we can't find your favorite book here!");
            }
        })
    });
});
function like(){
    var x = parseInt(event.currentTarget.parentNode.nextElementSibling.innerHTML) + 1
    console.log(like)
    event.currentTarget.parentNode.nextElementSibling.innerHTML = x
    var like = event.currentTarget.parentNode.nextElementSibling.innerHTML
    var id = event.currentTarget.parentNode.nextElementSibling.nextElementSibling.innerHTML
    var publisher = event.currentTarget.parentNode.previousElementSibling.innerHTML
    var authors = event.currentTarget.parentNode.previousElementSibling.previousElementSibling.innerHTML
    var title = event.currentTarget.parentNode.previousElementSibling.previousElementSibling.previousElementSibling.innerHTML
    var cover = event.currentTarget.parentNode.previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.innerHTML
    const book ={
        id : id,
        cover : cover,
        title : title,
        publisher : publisher,
        author : authors,
        likes : like
    }
    console.log(book)
    $.ajax({
        type:"POST",
        url:addLike_url,
        data:book
    })
}

$(document).ready(function(){
    $("modalbutton").on("click", function(z){
        $.ajax({
            url: top_book_url,
            success: function(book){
                $('#top5books').html('')
                var result = '';
                for(var i = 0; i < book.top5books.length; i++){
                    console.log(book.top5books[i].cover)
                    result+= "No:" + parseInt(i+1) +"<br>"+ 
                    "Title: "+book.top5books[i].title +"<br>"+
                    "Author: "  +book.top5books[i].author + "<br>" +
                    "Cover: "+ book.top5books[i].cover + "<br>"+
                    "Publisher: "+book.top5books[i].publisher + "<br>" +
                    "Numbers of Likes: " + book.top5books[i].likes + "<br>"
                }
                $('#top5books').append(result);
            }
        })
    })
});
